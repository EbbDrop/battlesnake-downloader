# Battlesnake Downloader

Firefox extension for downloading [battlesnake](https://play.battlesnake.com) games.

## Installation

### Firefox AddOn Store

* [https://addons.mozilla.org/en-US/firefox/addon/battlesnake-downloader/](https://addons.mozilla.org/en-US/firefox/addon/battlesnake-downloader/)

### Download latest build

* [battlesnake-downloader.zip](https://gitlab.com/l4r0x/battlesnake-downloader/-/jobs/artifacts/master/download?job=deploy)

Install it by selecting `about:addons` > `Install Add-On From File...`.

### Testing

Clone the repository and open `about:debugging` > `This Firefox` > `Load Temporary Add-on`.
Navigate to the repository directory and select the `manifest.json`.
